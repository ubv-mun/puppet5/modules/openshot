# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include openshot::install
class openshot::install {
  package { $openshot::package_name:
    ensure => $openshot::package_ensure,
  }
}
